package it.dtk.stackowerflow

import it.dtk.stackoverflow.hbase.HbaseConverter._
import it.dtk.stackoverflow.Loaders._
import it.nerdammer.spark.hbase._
import org.apache.spark.{ SparkConf, SparkContext }

/**
 * Created by fabiofumarola on 18/02/15.
 */
object SparkSaveUserTest extends App {

  val minSplits = 1
  val inputFile = "src/test/resources/Users.xml"

  val sparkConf = new SparkConf()
  sparkConf.set("spark.hbase.host", "localhost")
  val sc = new SparkContext("local", "Main", sparkConf)

  val rawData = sc.textFile(inputFile, minSplits)
  val usersData = rawData.flatMap(UserLoader.parse)

  usersData.toHBaseTable("users")
    .inColumnFamily("info")
    .save()
}
