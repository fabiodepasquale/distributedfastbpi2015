package it.kdde.distributed.sequentialpatterns

import it.kdde.distributed.sequentialpatterns.DFast.AssociationAlgorithm
import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.Logging
import org.apache.spark.annotation.Experimental
import org.apache.spark.rdd.RDD

/**
 * Created by fabiofumarola on 25/02/15.
 */
@Experimental
class DFast(algorithm: AssociationAlgorithm.Value, minSupp: Float, confidence: Float) extends Serializable with Logging {

  def run(input: RDD[String]): RDD[(String, Seq[Seq[ListNode]])] = {
    println("Entered DFast.run") //Iovine
    //load dataset and mine sequences
    val patterns = input.mapPartitions { partition =>
      println("Processing new partition") //Iovine
      val dataset: Dataset = Dataset.load(partition.toList, minSupp, confidence)

      val result = algorithm match {
        case AssociationAlgorithm.Frequent =>
          val dFast = DistributedFast(dataset)
          dFast.getFrequentSequences()

        case AssociationAlgorithm.Contiguous =>
          val dFast = DistributedContinuousFast(dataset)
          dFast.getContiguousSequences()

        case AssociationAlgorithm.Opening =>
          val dFast = DistributedContinuousFast(dataset)
          dFast.getOpeningSequences()
      }

      result.iterator

    }
    patterns
  }

}

object DFast extends Serializable with Logging {

  object AssociationAlgorithm extends Enumeration {
    type Algorithm = Value
    val Frequent, Opening, Contiguous = Value

    def fromString(name: String) =
      name.toLowerCase match {
        case "frequent" => Frequent
        case "opening" => Opening
        case "contiguous" => Contiguous
      }

  }

  def train(
    input: RDD[String],
    algorithm: AssociationAlgorithm.Value,
    minSupp: Float,
    confidence: Float): RDD[(String, Seq[Seq[ListNode]])] = {
    new DFast(algorithm, minSupp, confidence).run(input)
  }

}
