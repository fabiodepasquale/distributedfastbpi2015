package it.kdde.distributed.preprocessing

import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods.parse

import scala.collection.mutable

/**
  * Created by isz_d on 17/06/2017.
  */
object XESPreprocessor2 extends Serializable with Preprocessor  {
  var questionMap: Map[String, Int] = _
  var monitoringResourceMap: Map[String, Int] = _
  var resourceMap: Map[String, Int] = _
  var transitionMap: Map[String, Int] = _

  var path: String = _

  case class Row(
                monitoringResource: String,
                resource: String,
                transition: String,
                classificationValue: Option[Double] = None,
                regressionValue: Option[Double] = None
                )

  //Always ignore classificationValue and regressionValue
  val featuresToIgnore = List(3, 4)

  //Number of features (excluded classificationValue and regressionValue)
  override def numberFeatures: Int = 3

  def initialize(): Unit = {
    var jsonString = scala.io.Source.fromFile(path + "_question.txt").mkString
    questionMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_monitoringResource.txt").mkString
    monitoringResourceMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_resource.txt").mkString
    resourceMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_transition.txt").mkString
    transitionMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
  }

  /**
    * Vengono usati per trasformare ogni sequenza di  test in un Vettore di feature da dare in input al classificatore/regressore associato
    * @param row in the form tid1 :: {samedata} -1 tid2 :: {samedata} -1 -2
    * @return the vector of feature associate to sequence row
    */
  override def extractFeatureSequence(row: String): Vector = {
    implicit val formats = DefaultFormats
    val listTuples = row.split(" -1 ")

    //take the first n - 1 to remove the element with -2
    val array = listTuples.take(listTuples.length - 1).map { elem =>
      val pair = elem.split(" :: ")
      val row = parse(pair(1)).extract[Row]

      try {
        var transition = transitionMap.get(row.transition).get.asInstanceOf[BigInt].bigInteger.doubleValue()
      } catch {
        case e: NoSuchElementException => {
          println("got NoSuchElementException, cause is " + row.monitoringResource)
        }
      }
      //For each categorical attribute, returns the associated number in the respective map
      Array(
        monitoringResourceMap.get(row.monitoringResource).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        resourceMap.get(row.resource).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        transitionMap.get(row.transition).get.asInstanceOf[BigInt].bigInteger.doubleValue()
      )

    }.reduce(_ ++ _)

    Vectors.dense(array)
  }

  //Returns a map that contains, for each categorical feature's index, the number of distinct values of that feature
  override def categoricalFeatures: Map[Int, Int] = {
    var catFeatureInfo = new mutable.HashMap[Int, Int]
    //catFeatureInfo += (1 -> questionMap.size)
    catFeatureInfo += (0 -> monitoringResourceMap.size)
    catFeatureInfo += (1 -> resourceMap.size)
    catFeatureInfo += (2 -> transitionMap.size)

    catFeatureInfo.toMap
  }

  /*
  Returns a labeled point, in the form (label, [attributes]), that is used for regression
 */
  override def getLabeledPointForRegression(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    val rowLabel = parse(row.last.getData).extract[Row]
    val label = rowLabel.regressionValue.get

    val rowVector = row.flatMap { listNode =>
      val data = parse(row.last.getData).extract[Row]

      val newData = Row(
        monitoringResourceMap.get(data.monitoringResource).get.toString,
        resourceMap.get(data.resource).get.toString,
        transitionMap.get(data.transition).get.toString,
        data.classificationValue,
        data.regressionValue
      )

      val r = Row.unapply(newData).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      filtered.map(_._1.toString.toDouble)

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  /*
  Returns a labeled point, in the form (label, [attributes]), that is used for classification
   */
  override def getLabeledPointForClassification(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    var rowLabel: Row = null
    try {
      rowLabel = parse(row.last.getData).extract[Row]
    } catch {
      case e: Exception => {
        println("cause is " + row.last.getData)
        e.printStackTrace()
      }
    }
    val label = rowLabel.classificationValue.get

    val rowVector = row.flatMap { listNode =>
      println("row is " + row.last.getData)
      var data: Row = null
      try {
        data = parse(row.last.getData).extract[Row]
      } catch {
        case e: Exception => {
          println("cause is " + row)
          e.printStackTrace()
        }
      }

      //val data = rawData.copy(requestType = requestCodeMap.get(rawData.requestType).get.toString)
      val newData = Row(
        monitoringResourceMap.get(data.monitoringResource).get.toString,
        resourceMap.get(data.resource).get.toString,
        transitionMap.get(data.transition).get.toString,
        data.classificationValue,
        data.regressionValue
      )

      val r = Row.unapply(newData).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      var prova: List[Double] = null
      try {
        prova = filtered.map(_._1.toString.toDouble)
      } catch {
        case e: Exception => {
          println("Got Exception. Cause is " + r)
          e.printStackTrace()
        }
      }
      prova

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  override def generateSequenceDataset(lines: RDD[String], minLength: Int)(implicit sc: SparkContext): RDD[String] = ???

  /**
    *
    * @param json return the value of the labelClassification contained in the json
    */
  override def getLabelClassification(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Row]
    sequence.classificationValue.get
  }

  override def getLabelRegression(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Row]
    sequence.regressionValue.get
  }
}
