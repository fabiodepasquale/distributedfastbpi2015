package it.kdde.distributed.preprocessing

import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD

/**
 * Created by fabiana on 3/24/15.
 */
trait Preprocessor extends Serializable {
  def extractFeatureSequence(row: String): Vector
  //number feature that will be considered for prediction
  def numberFeatures: Int
  def categoricalFeatures: Map[Int, Int]

  def getLabeledPointForRegression(row: Seq[ListNode]): LabeledPoint
  def getLabeledPointForClassification(row: Seq[ListNode]): LabeledPoint

  //FIXME attribute lines is not used in StackExchangePreprocessor
  def generateSequenceDataset(lines: RDD[String], minLength: Int)(implicit sc: SparkContext): RDD[String]


  /**
   *
   * @param json return the value of the labelClassification contained in the json
   */
  def getLabelClassification(json: String): Double
  def getLabelRegression(json: String): Double
}
