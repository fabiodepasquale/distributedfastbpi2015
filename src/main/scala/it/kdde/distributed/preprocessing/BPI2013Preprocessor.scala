package it.kdde.distributed.preprocessing

import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods.parse

import scala.collection.mutable

/**
  * Preprocessor class for BPI 2013 Dataset
  *
  * NOTICE: In the experimentation this class must be modified in the following ways:
  *
  * Incidents: The 'role' attribute must be uncommented, the number of features is 6,
  * and the ignored attributes are (6, 7)
  *
  * Open and Closed Problems: The 'role' attribute must be commented, the number of
  * features is 5, ignored attributes are (5, 6)
  */
object BPI2013Preprocessor extends Serializable with Preprocessor  {
  var resourceCountryMap: Map[String, Int] = _
  var organizationCountryMap: Map[String, Int] = _
  var organizationInvolvedMap: Map[String, Int] = _
  var roleMap: Map[String, Int] = _
  var impactMap: Map[String, Int] = _
  var transitionMap: Map[String, Int] = _
  var catFeatureMaps: mutable.HashMap[Int, Map[String, Int]] = new mutable.HashMap[Int, Map[String, Int]]
  var features: mutable.HashMap[Int, String] = new mutable.HashMap[Int, String]
  var eventIdMap: mutable.HashMap[Int, String] = new mutable.HashMap[Int, String]()

  var path: String = _

  case class Row(
                resource_country: String,
                organization_country: String,
                //organization_involved: String,
                impact: String,
                transition: String,
                //role: String,
                timeElapsed: Long,
                classificationValue: Option[Double] = None,
                regressionValue: Option[Double] = None
                )

  val featuresToIgnore = List(5, 6)

  override def numberFeatures: Int = 5

  def initialize(): Unit = {
    var jsonString = scala.io.Source.fromFile(path + "_impact.txt").mkString
    impactMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_transition.txt").mkString
    transitionMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_organization_country.txt").mkString
    organizationCountryMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    //jsonString = scala.io.Source.fromFile(path + "_organization_involved.txt").mkString
    //organizationInvolvedMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_resource_country.txt").mkString
    resourceCountryMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_role.txt").mkString
    roleMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]

    catFeatureMaps += (0 -> resourceCountryMap)
    catFeatureMaps += (1 -> organizationCountryMap)
    catFeatureMaps += (2 -> impactMap)
    catFeatureMaps += (3 -> transitionMap)
    //catFeatureMaps += (4 -> roleMap)

    features += (0 -> "resource_country")
    features += (1 -> "organization_country")
    features += (2 -> "impact")
    features += (3 -> "transition")
    //features += (4 -> "role")
    features += (4 -> "timeElapsed")

    eventIdMap += (1 -> "Accepted")
    eventIdMap += (2 -> "Queued")
    eventIdMap += (3 -> "Completed")
    eventIdMap += (4 -> "Unmatched")
  }

  /**
    * Vengono usati per trasformare ogni sequenza di  test in un Vettore di feature da dare in input al classificatore/regressore associato
    * @param row in the form tid1 :: {samedata} -1 tid2 :: {samedata} -1 -2
    * @return the vector of feature associate to sequence row
    */
  override def extractFeatureSequence(row: String): Vector = {
    implicit val formats = DefaultFormats
    val listTuples = row.split(" -1 ")

    //take the first n - 1 to remove the element with -2
    val array = listTuples.take(listTuples.length - 1).map { elem =>
      val pair = elem.split(" :: ")
      val row = parse(pair(1)).extract[Row]

      Array(
        /*monitoringResourceMap.getOrElse(row.monitoringResource, 0).toDouble,
        resourceMap.getOrElse(row.resource, 0).toDouble,
        transitionMap.getOrElse(row.transition, 0).toDouble*/
        resourceCountryMap.get(row.resource_country).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        organizationCountryMap.get(row.organization_country).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        //organizationInvolvedMap.get(row.organization_involved).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        impactMap.get(row.impact).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        transitionMap.get(row.transition).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        //roleMap.get(row.role).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        row.timeElapsed
      )

    }.reduce(_ ++ _)

    Vectors.dense(array)
  }


  override def categoricalFeatures: Map[Int, Int] = {
    var catFeatureInfo = new mutable.HashMap[Int, Int]
    catFeatureInfo += (0 -> resourceCountryMap.size)
    catFeatureInfo += (1 -> organizationCountryMap.size)
    //catFeatureInfo += (2 -> organizationInvolvedMap.size)
    catFeatureInfo += (2 -> impactMap.size)
    catFeatureInfo += (3 -> transitionMap.size)
    //catFeatureInfo += (4 -> roleMap.size)
    catFeatureInfo.toMap
  }

  override def getLabeledPointForRegression(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    val rowLabel = parse(row.last.getData).extract[Row]
    val label = rowLabel.regressionValue.get

    val rowVector = row.flatMap { listNode =>
      val data = parse(row.last.getData).extract[Row]
      
      val newData = Row(
        resourceCountryMap.get(data.resource_country).get.toString,
        organizationCountryMap.get(data.organization_country).get.toString,
        //organizationInvolvedMap.get(data.organization_involved).get.toString,
        impactMap.get(data.impact).get.toString,
        transitionMap.get(data.transition).get.toString,
        //roleMap.get(data.role).get.toString,
        data.timeElapsed,
        data.classificationValue,
        data.regressionValue
      )

      val r = Row.unapply(newData).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      filtered.map(_._1.toString.toDouble)

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }


  override def getLabeledPointForClassification(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    var rowLabel: Row = null
    try {
      rowLabel = parse(row.last.getData).extract[Row]
    } catch {
      case e: Exception => {
        println("cause is " + row.last.getData)
        e.printStackTrace()
      }
    }
    val label = rowLabel.classificationValue.get

    val rowVector = row.flatMap { listNode =>
      //println("row is " + row.last.getData)
      var data: Row = null
      try {
        data = parse(row.last.getData).extract[Row]
      } catch {
        case e: Exception => {
          println("cause is " + row)
          e.printStackTrace()
        }
      }

      //val data = rawData.copy(requestType = requestCodeMap.get(rawData.requestType).get.toString)
      val newData = Row(
        resourceCountryMap.get(data.resource_country).get.toString,
        organizationCountryMap.get(data.organization_country).get.toString,
        //organizationInvolvedMap.get(data.organization_involved).get.toString,
        impactMap.get(data.impact).get.toString,
        transitionMap.get(data.transition).get.toString,
        //roleMap.get(data.role).get.toString,
        data.timeElapsed,
        data.classificationValue,
        data.regressionValue
      )

      val r = Row.unapply(newData).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      var prova: List[Double] = null
      try {
        prova = filtered.map(_._1.toString.toDouble)
      } catch {
        case e: Exception => {
          println("Got Exception. Cause is " + r)
          e.printStackTrace()
        }
      }
      prova

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  override def generateSequenceDataset(lines: RDD[String], minLength: Int)(implicit sc: SparkContext): RDD[String] = ???

  /**
    *
    * @param json return the value of the labelClassification contained in the json
    */
  override def getLabelClassification(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Row]
    sequence.classificationValue.get
  }

  override def getLabelRegression(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Row]
    sequence.regressionValue.get
  }
}
