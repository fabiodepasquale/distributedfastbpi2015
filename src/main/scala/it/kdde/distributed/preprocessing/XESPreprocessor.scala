package it.kdde.distributed.preprocessing
import java.io.File
import java.text.SimpleDateFormat
import java.util
import java.util.{Calendar, GregorianCalendar, Locale}

import it.kdde.distributed.sequentialpatterns.Dataset
import it.kdde.sequentialpatterns.model.ListNode
import org.apache.spark.SparkContext
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.json4s.{DefaultFormats, MappingException}
import org.json4s.jackson.JsonMethods.parse

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.xml.Node

/**
  * Created by isz_d on 17/06/2017.
  */
object XESPreprocessor extends Serializable with Preprocessor  {
  var questionMap: Map[String, Int] = _
  var monitoringResourceMap: Map[String, Int] = _
  var resourceMap: Map[String, Int] = _
  var transitionMap: Map[String, Int] = _
  var caseStatusMap: Map[String, Int] = _
  var lastPhaseMap: Map[String, Int] = _
  var caseTypeMap: Map[String, Int] = _
  var requestCompleteMap: Map[String, Int] = _

  var path: String = _

  case class Row(
                monitoringResource: String,
                resource: String,
                transition: String,
                caseStatus: String,
                last_phase: String,
                case_type: String,
                requestComplete: String,
                classificationValue: Option[Double] = None,
                regressionValue: Option[Double] = None
                )

  val featuresToIgnore = List(7, 8)

  override def numberFeatures: Int = 7

  def initialize(): Unit = {
    var jsonString = scala.io.Source.fromFile(path + "_question.txt").mkString
    questionMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_monitoringResource.txt").mkString
    monitoringResourceMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_resource.txt").mkString
    resourceMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_transition.txt").mkString
    transitionMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_caseStatus.txt").mkString
    caseStatusMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_last_phase.txt").mkString
    lastPhaseMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_case_type.txt").mkString
    caseTypeMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
    jsonString = scala.io.Source.fromFile(path + "_requestComplete.txt").mkString
    requestCompleteMap = parse(jsonString).values.asInstanceOf[Map[String, Int]]
  }

  /**
    * Vengono usati per trasformare ogni sequenza di  test in un Vettore di feature da dare in input al classificatore/regressore associato
    * @param row in the form tid1 :: {samedata} -1 tid2 :: {samedata} -1 -2
    * @return the vector of feature associate to sequence row
    */
  override def extractFeatureSequence(row: String): Vector = {
    implicit val formats = DefaultFormats
    val listTuples = row.split(" -1 ")

    //take the first n - 1 to remove the element with -2
    val array = listTuples.take(listTuples.length - 1).map { elem =>
      val pair = elem.split(" :: ")
      val row = parse(pair(1)).extract[Row]

      try {
        var transition = transitionMap.get(row.transition).get.asInstanceOf[BigInt].bigInteger.doubleValue()
      } catch {
        case e: NoSuchElementException => {
          println("got NoSuchElementException, cause is " + row.monitoringResource)
        }
      }

      Array(
        /*monitoringResourceMap.getOrElse(row.monitoringResource, 0).toDouble,
        resourceMap.getOrElse(row.resource, 0).toDouble,
        transitionMap.getOrElse(row.transition, 0).toDouble*/
        monitoringResourceMap.get(row.monitoringResource).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        resourceMap.get(row.resource).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        transitionMap.get(row.transition).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        caseStatusMap.get(row.caseStatus).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        lastPhaseMap.get(row.last_phase).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        caseTypeMap.get(row.case_type).get.asInstanceOf[BigInt].bigInteger.doubleValue(),
        requestCompleteMap.get(row.requestComplete).get.asInstanceOf[BigInt].bigInteger.doubleValue()
      )

    }.reduce(_ ++ _)

    Vectors.dense(array)
  }


  override def categoricalFeatures: Map[Int, Int] = {
    var catFeatureInfo = new mutable.HashMap[Int, Int]
    //catFeatureInfo += (1 -> questionMap.size)
    catFeatureInfo += (0 -> monitoringResourceMap.size)
    catFeatureInfo += (1 -> resourceMap.size)
    catFeatureInfo += (2 -> transitionMap.size)
    catFeatureInfo += (3 -> caseStatusMap.size)
    catFeatureInfo += (4 -> lastPhaseMap.size)
    catFeatureInfo += (5 -> caseTypeMap.size)
    catFeatureInfo += (6 -> requestCompleteMap.size)

    catFeatureInfo.toMap
  }

  override def getLabeledPointForRegression(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    val rowLabel = parse(row.last.getData).extract[Row]
    val label = rowLabel.regressionValue.get

    val rowVector = row.flatMap { listNode =>
      val data = parse(row.last.getData).extract[Row]

      val newData = Row(
        monitoringResourceMap.get(data.monitoringResource).get.toString,
        resourceMap.get(data.resource).get.toString,
        transitionMap.get(data.transition).get.toString,
        caseStatusMap.get(data.caseStatus).get.toString,
        lastPhaseMap.get(data.last_phase).get.toString,
        caseTypeMap.get(data.case_type).get.toString,
        requestCompleteMap.get(data.requestComplete).get.toString,
        data.classificationValue,
        data.regressionValue
      )

      val r = Row.unapply(newData).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      filtered.map(_._1.toString.toDouble)

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }


  override def getLabeledPointForClassification(row: Seq[ListNode]): LabeledPoint = {
    implicit val formats = DefaultFormats
    var rowLabel: Row = null
    try {
      rowLabel = parse(row.last.getData).extract[Row]
    } catch {
      case e: Exception => {
        println("cause is " + row.last.getData)
        e.printStackTrace()
      }
    }
    val label = rowLabel.classificationValue.get

    val rowVector = row.flatMap { listNode =>
      println("row is " + row.last.getData)
      var data: Row = null
      try {
        data = parse(row.last.getData).extract[Row]
      } catch {
        case e: Exception => {
          println("cause is " + row)
          e.printStackTrace()
        }
      }

      //val data = rawData.copy(requestType = requestCodeMap.get(rawData.requestType).get.toString)
      val newData = Row(
        monitoringResourceMap.get(data.monitoringResource).get.toString,
        resourceMap.get(data.resource).get.toString,
        transitionMap.get(data.transition).get.toString,
        caseStatusMap.get(data.caseStatus).get.toString,
        lastPhaseMap.get(data.last_phase).get.toString,
        caseTypeMap.get(data.case_type).get.toString,
        requestCompleteMap.get(data.requestComplete).get.toString,
        data.classificationValue,
        data.regressionValue
      )

      val r = Row.unapply(newData).get
      val indexedValues = r.productIterator.toList.zip(0 until r.productArity)

      val filtered = indexedValues.filterNot(kv => featuresToIgnore.contains(kv._2))
      var prova: List[Double] = null
      try {
        prova = filtered.map(_._1.toString.toDouble)
      } catch {
        case e: Exception => {
          println("Got Exception. Cause is " + r)
          e.printStackTrace()
        }
      }
      prova

    }.toArray

    LabeledPoint(label, Vectors.dense(rowVector))
  }

  override def generateSequenceDataset(lines: RDD[String], minLength: Int)(implicit sc: SparkContext): RDD[String] = ???

  /**
    *
    * @param json return the value of the labelClassification contained in the json
    */
  override def getLabelClassification(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Row]
    sequence.classificationValue.get
  }

  override def getLabelRegression(json: String): Double = {
    implicit val formats = DefaultFormats
    val sequence= parse(json).extract[Row]
    sequence.regressionValue.get
  }
}
