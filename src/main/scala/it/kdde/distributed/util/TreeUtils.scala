package it.kdde.distributed.util

import it.kdde.distributed.preprocessing.BPI2013Preprocessor
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.configuration.FeatureType
import org.apache.spark.mllib.tree.model.{DecisionTreeModel, Node}

/**
  * Created by isz_d on 13/07/2017.
  */
object TreeUtils {
  //Returns a string containing the decision tree
  def printTree(t: DecisionTreeModel, mapIntClass: Map[Int, Double]): String = {
    val topNode = t.topNode
    return nodeToString(topNode, 0, mapIntClass)
  }

  def nodeToString(n: Node, level: Int, mapIntClass: Map[Int, Double]): String = {
    val indent = "  " * level
    val catFeatureMaps = BPI2013Preprocessor.catFeatureMaps
    val features = BPI2013Preprocessor.features
    var categoriesString = ""

    var result: String = ""
    if (n.isLeaf) {
      if (mapIntClass != null) {
        result += "Predict " + BPI2013Preprocessor.eventIdMap(mapIntClass(n.predict.predict.toInt).toInt)
      } else {
        result += "Predict " + n.predict
      }
    }
    if (!n.split.isEmpty) {
      val split = n.split.get
      val splitFeatureIndex = split.feature % BPI2013Preprocessor.numberFeatures
      val eventIndex = split.feature / BPI2013Preprocessor.numberFeatures

      if (split.featureType.equals(FeatureType.Categorical)) {

        for (category <- split.categories) {
          categoriesString += getKey(catFeatureMaps(splitFeatureIndex), category.toInt) + ", "
        }
        result += "If (Event " + eventIndex + ":" + features(splitFeatureIndex) + " in {" + categoriesString + "})"
      } else {
        result += "If (Event " + eventIndex + ":" + features(splitFeatureIndex) + " > " + split.threshold + ")"
      }

      if (!n.leftNode.isEmpty) {
        result += "\n" + indent + "  " + nodeToString(n.leftNode.get, level + 1, mapIntClass)
      }
      if (!n.rightNode.isEmpty) {
        if (split.featureType.equals(FeatureType.Categorical)) {
          result += "\n" + indent + "Else if (Event " + eventIndex + ":" + features(splitFeatureIndex) + " not in {" + categoriesString + "})"
        } else {
          result += "\n" + indent + "Else if (Event " + eventIndex + ":" + features(splitFeatureIndex) + " <= " + split.threshold + ")"
        }
        result += "\n" + indent + "  " + nodeToString(n.rightNode.get, level + 1, mapIntClass)
      }
    }
    return result
  }

  def getKey(map: Map[String, Int], value: BigInt): String = {
    for (key <- map.keys) {
      if (map(key) == value) {
        return key
      }
    }
    return null
  }
}
