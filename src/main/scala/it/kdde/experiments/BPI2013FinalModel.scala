package it.kdde.experiments

import java.io.{BufferedWriter, File, FileWriter}

import it.kdde.distributed.preprocessing.BPI2013Preprocessor
import it.kdde.distributed.sequentialpatterns.{DFast, PmmWithFast}
import it.kdde.distributed.util.{TreeUtils, Util}
import it.kdde.util.Statistics
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Properties

/**
  * This class is used to train the final model for the BPI Challenge 2013 dataset
  * It returns a pmmWithFastModel trained with all the data in the dataset
  *
  */
object BPI2013FinalModel extends App with Serializable {

  args match {
    case Array(algorithm, inputFile, numPartitions, minSupp, confidence, maxDepthEvaluation,  statisticsFile) =>

      val master = Properties.envOrElse("MASTER", "local")
      val conf = new SparkConf().setAppName("PmmWithFastBPI2015")
        .set("spark.executor.memory", "2g")
        .set("spark.driver.memory", "2g")
        .set("spark.serialization","org.apache.spark.serializer.KyroSerializer")
        .set("spark.io.compression.codec","org.apache.spark.io.SnappyCompressionCodec")
      implicit val sc = new SparkContext(master, "PmmWithFast", conf)

      val algorithmType = DFast.AssociationAlgorithm.fromString(algorithm)
      val lines = sc.textFile(inputFile)
      //Load the dataset
      val dataset = sc.textFile(inputFile, numPartitions.toInt)
      val statistics = new Statistics

      BPI2013Preprocessor.path = inputFile
      BPI2013Preprocessor.initialize()

      println("Start training process")
      statistics.startTimeSequence

      //Train using PmmWithFast
      val pmmWithFastModel = PmmWithFast.train(
        associationAlgo = algorithmType,
        minSupp = minSupp.toFloat,
        confidence = confidence.toFloat,
        categoricalFeaturesInfo = BPI2013Preprocessor.categoricalFeatures,
        input = dataset,
        BPI2013Preprocessor
      )
      statistics.endTimeSequence

      val time = statistics.sequenceTime()

      sc.stop()

      //Print found sequences

      val sequencesFile = new File(statisticsFile + "/sequences.txt")
      sequencesFile.getParentFile.mkdirs()
      var bw = new BufferedWriter(new FileWriter(sequencesFile))
      for (sequence <- pmmWithFastModel.patterns) {
        bw.write(sequenceToString(sequence) + "\n")
      }
      bw.close()

      //Print classifiers
      val classifiersFile = new File(statisticsFile + "/classifiers.txt")
      classifiersFile.getParentFile.mkdirs()
      bw = new BufferedWriter(new FileWriter(classifiersFile))
      for (key <- pmmWithFastModel.mapClassifiers) {
        bw.write("\n\nSequence is " + sequenceToString(key._1) + "\n")
        bw.write(TreeUtils.printTree(key._2.model.get, key._2.mapIntClass))
      }
      bw.close()

      //Print regressors
      val regressorsFile = new File(statisticsFile + "/regressors.txt")
      regressorsFile.getParentFile.mkdirs()
      bw = new BufferedWriter(new FileWriter(regressorsFile))
      for (key <- pmmWithFastModel.mapRegressors) {
        bw.write("\n\nSequence is " + sequenceToString(key._1) + "\n")
        bw.write(TreeUtils.printTree(key._2.model, null))
      }
      bw.close()

    case a =>

      println(
        s"""
           |Insert
           | - Algorithm Type:
           |   - "frequent"
           |   - "opening"
           |   - "contiguous"
           | - inputFile path
           | - num. partitions (e.g. 2)
           | - min. support (e.g. 0.01)
           | - confidence (e.g. 0.95)
           | - max depth evaluation for test sequences
           | - statistics file absolute path
           |
          |   all the parameters should be defined in a single line with spaces
           |
          |   for  $a
        """.stripMargin)
  }

  /*
   * Given a sequence with numeric event IDs, it returns a string representing the sequence with the proper
   * event names.
   */
  def sequenceToString(sequence: String): String = {
    var result = ""

    val splitSequence = sequence.split(" ")

    for (i <- splitSequence.indices) {
      if (splitSequence(i).equals("-1")) {
        if (i < (splitSequence.size - 2)) {
          result += ","
        }
      } else if (splitSequence(i).equals("-2")) {
        result += "."
      } else {
        result += BPI2013Preprocessor.eventIdMap(splitSequence(i).toInt)
      }
    }

    result
  }


}
