package it.kdde.experiments

import it.kdde.distributed.preprocessing.{Preprocessor, StackExchangePreprocessor}
import it.kdde.distributed.sequentialpatterns.{DFast, PmmWithFast}
import it.kdde.distributed.util.Util
import it.kdde.util.Statistics
import org.apache.spark.{SparkConf, SparkContext}

import scala.util.Properties

/**
 * Created by fabiana on 3/24/15.
 */
object StackExchangeMain extends App with Serializable {
  args match {
    case Array(inputFile, numPartitions, minSupp, confidence, statisticsFile) =>

      val master = Properties.envOrElse("MASTER", "local")
      //Using local as the default value for the master machine makes it easy to launch your application locally in a test environment.
      val conf = new SparkConf().setAppName("PmmWithFast")
      implicit val sc = new SparkContext(master, "PmmWithFast", conf)
      val preprocessor: Preprocessor = StackExchangePreprocessor

      //val dataset = preprocessor.generateSequenceDataset(null, 2)
      val dataset = sc.textFile(inputFile, numPartitions.toInt)
      val splits = dataset.randomSplit(Array(0.7, 0.3))
      val trainingData = splits(0)
      val testingData = splits(1)
      val statistics = new Statistics

      println("Start training process")
      statistics.startTimeSequence
      val pmmWithFastModel = PmmWithFast.train(
        associationAlgo = DFast.AssociationAlgorithm.Frequent,
        minSupp = minSupp.toFloat,
        confidence = confidence.toFloat,
        categoricalFeaturesInfo = preprocessor.categoricalFeatures,
        input = trainingData,
        preprocessor
      )
      statistics.endTimeSequence
      val time = statistics.sequenceTime()
      println("End training process")

      println("Start testing process")
      val predictions = pmmWithFastModel.test(testingData).cache()
      val results = PmmWithFast.computePerformance(predictions)
      println("Start testing process")

      Util.printStat(inputFile, numPartitions, minSupp, confidence, results, time, pmmWithFastModel, statisticsFile)
      sc.stop()

    case _ =>
      println("Insert inputFile path, num. partition (e.g. 2), min. support (e.g. 0.01) and confidence (e.g. 0.95)")
  }


}
