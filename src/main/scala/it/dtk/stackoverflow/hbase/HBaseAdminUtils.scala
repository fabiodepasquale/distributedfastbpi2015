package it.dtk.stackoverflow.hbase

import it.dtk.stackoverflow.Model.Answer
import it.nerdammer.spark.hbase.HBaseSparkConf
import org.apache.hadoop.conf.Configured
import org.apache.hadoop.hbase.client.{Get, HBaseAdmin, HTable, Result}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.{HBaseConfiguration, HColumnDescriptor, HTableDescriptor, TableName}

import scala.language.implicitConversions

/**
 * Created by fabiofumarola on 21/02/15.
 */
object HBaseAdminUtils extends Serializable  {

//  val conf = HBaseSparkConf()
//
  val conf = HBaseConfiguration.create()
  conf.addResource("hbase-default.xml")
  conf.addResource("hbase-site.xml")

  val admin = new HBaseAdmin(conf)
  val sequencesTable = new HTable(conf, "sequences")

  def createTable(table: String, columnFamilies: List[String]): Unit = {

    val tableName = TableName.valueOf(table)

    if (admin.tableExists(tableName)) {
      dropTable(table)
    }

    val tableDesc = new HTableDescriptor(TableName.valueOf(table))
    columnFamilies.foreach { cf =>
      tableDesc.addFamily(new HColumnDescriptor(cf))
    }
    admin.createTable(tableDesc)
  }

  def dropTable(table: String): Unit = {
    val tableName = TableName.valueOf(table)
    admin.disableTable(tableName)
    admin.deleteTable(tableName)
  }

  /**
   * Attenzione agli ultimi 2 elementi
   * @param rowKey
   * @return
   */
  def getCellElem(rowKey: String): Answer = {

    val cfPost = Bytes.toBytes("post")
    val cfUser = Bytes.toBytes("user")

      implicit def toInt(x: Option[Boolean]): Int =
        x.map { v =>
          if (v) 1
          else 0
        }.getOrElse(0)

    val sid = rowKey.split(":")(0)
    val get = new Get(Bytes.toBytes(rowKey))
    val r: Result = sequencesTable.get(get)

    val answerId =  Option(r.getValue(cfPost, "id".getBytes)).map(Bytes.toInt)
    val reputation = Option(r.getValue(cfUser, "reputation".getBytes)).map(Bytes.toInt)
    val creationDate = Option(r.getValue(cfUser, "creationDate".getBytes)).map(Bytes.toLong)
    val displayName = Option(r.getValue(cfUser, "displayName".getBytes)).map(Bytes.toString).map(_.length > 0)
    val lastAccessDate = Option(r.getValue(cfUser, "lastAccessDate".getBytes)).map(Bytes.toLong)
    val websiteUrl = Option(r.getValue(cfUser, "websiteUrl".getBytes)).map(Bytes.toString).map(_.length > 0)
    val location = Option(r.getValue(cfUser, "location".getBytes)).map(Bytes.toString).map(_.length > 0)
    val aboutMe = Option(r.getValue(cfUser, "aboutMe".getBytes)).map(Bytes.toString).map(_.length > 0)
    val views = Option(r.getValue(cfUser, "views".getBytes)).map(Bytes.toInt)
    val upVotes = Option(r.getValue(cfUser, "upVotes".getBytes)).map(Bytes.toInt)
    val downVotes = Option(r.getValue(cfUser, "downVotes".getBytes)).map(Bytes.toInt)
    val profileImageUrl = Option(r.getValue(cfUser, "profileImageUrl".getBytes)).map(Bytes.toString).map(_.length > 0)
    val age = Option(r.getValue(cfUser, "age".getBytes)).map(Bytes.toInt)
    val postScore = Option(r.getValue(cfPost, "postScore".getBytes)).map(Bytes.toInt)
    val postViewCount = Option(r.getValue(cfPost, "postViewCount".getBytes)).map(Bytes.toInt)
    val commentCount = Option(r.getValue(cfPost, "commentCount".getBytes)).map(Bytes.toInt)

    Answer(
      reputation.getOrElse(0),
      creationDate.getOrElse(0).toString.toInt,
      displayName,
      lastAccessDate.getOrElse(0).toString.toInt,
      websiteUrl,
      location,
      aboutMe,
      views.getOrElse(0),
      upVotes.getOrElse(0),
      downVotes.getOrElse(0),
      profileImageUrl,
      age.getOrElse(0),
      postScore.getOrElse(0),
      postViewCount.getOrElse(0),
      commentCount.getOrElse(0),
      0,
      0,
      sid.toInt,
      answerId.getOrElse(0)
    )
  }

}
